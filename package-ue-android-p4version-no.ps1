#add-type -AssemblyName PresentationFramework

#$cwd=Get-Location
$cwd=$PSScriptRoot
#[System.Windows.MessageBox]::Show("$($cwd)")

Push-Location $cwd

$cmd="p4 changes -m1 ./...#have"
$result=Invoke-Expression $cmd

$head_changelist_no=$result.Split(" ")[1]
Write-Host $head_changelist_no

$DefaultEngineFile=(Get-Item ".\Config\DefaultEngine.ini").FullName

$checkout="p4 edit ./Config/DefaultEngine.ini"
Invoke-Expression $checkout

$WholeFile=Get-Content -Path $DefaultEngineFile -Encoding UTF8
for ($i=0; $i -lt $WholeFile.Count; $i++) {
    if ($WholeFile[$i] -match 'VersionDisplayName=') {
        $WholeFile[$i] = "VersionDisplayName=$($head_changelist_no)#$(Get-Date -Format "yyyy-MM-dd HH:mm")"
        break
    }
}
$WholeFile | Set-Content -Path $DefaultEngineFile -Encoding utf8

Pop-Location
