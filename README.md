# convert_bp_to_cpp.ps1
- 将Unreal Engine的Blueprint工程转为C++工程
- 用法
    - 将脚本文件拷贝到工程目录下，与uproject文件平级，然后执行该脚本即可
    - 脚本文件里的 **$FORCE_SAVE** 为 _true_ 时表示覆写已存在的文件
