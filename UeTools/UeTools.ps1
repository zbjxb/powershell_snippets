function getProgramDataPath {
    # param (
    #     [parameter-type]$Parameter1,
    #     [parameter-type]$Parameter2
    # )

    # Code to perform the desired tasks

    return $env:ProgramData
}

$applicationSettingsPath = Join-Path $(getProgramDataPath) "Epic"

$installedListFile = Join-Path $applicationSettingsPath "UnrealEngineLauncher" "LauncherInstalled.dat"
Write-Host $installedListFile

if (Test-Path -Path $installedListFile -PathType Leaf) {
    Write-Host "yes"
}
