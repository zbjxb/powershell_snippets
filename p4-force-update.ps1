﻿# p4-force-update.ps1
# 需拷贝到p4本地工程目录下使用

$revision = Read-Host "请输入revision版本号"
$revision_int = [int]$revision
if ($revision_int -gt 1) {
    $folder=(Get-Item .).FullName
    $param=$folder+"\...@$revision"
    $cmd="p4 sync -f $param"
    Write-Host $cmd
    Invoke-Expression $cmd
}