﻿$cwd = Split-Path $MyInvocation.MyCommand.Path -Parent
$plugins=$cwd+"\Plugins"

gci $plugins -Recurse | where { ($_.Extension -eq ".uplugin") -or ($_.Extension -eq ".uplugin-") } | ForEach-Object { 
    if ($_.Extension -eq ".uplugin") {
        Write-Host "Rename $($_.Name) to $($_.Name+"-")"
        Rename-Item -Path $_.FullName -NewName ($_.Name+"-")
    }
    else {
        Write-Host "Rename $($_.Name) to $($_.BaseName+".uplugin")"
        Rename-Item -Path $_.FullName -NewName ($_.BaseName+".uplugin")
    }
}