﻿$env:path+=";F:\UE4_Projects\Car\Tools\VideoTools;"

# make first silence audio
$silence_seconds=13
$make_silence_cmd="ffmpeg -y -f lavfi -i anullsrc=channel_layout=stereo:sample_rate=16000 -t ${silence_seconds} silence_${silence_seconds}_sec.wav"
Invoke-Expression $make_silence_cmd

$merge_cmd="ffmpeg -y -i silence_${silence_seconds}_sec.wav -i 5.wav -filter_complex `"[0:a][1:a]concat=n=2:v=0:a=1`" -f wav -ac 2 -ar 16000 5-1VO.wav"
Invoke-Expression $merge_cmd